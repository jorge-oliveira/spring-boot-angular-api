package pt.jorge.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import pt.jorge.model.Book;

public interface BookRepository extends JpaRepository<Book, Long> {

}
