package pt.jorge.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import pt.jorge.model.Book;
import pt.jorge.service.BookService;

@CrossOrigin("*")
@RestController
public class BookController {

	@Autowired
	private BookService bookService;

	// get all the books
	@GetMapping("/api/book")
	public ResponseEntity<List<Book>> list() {

		// get the list of books
		List<Book> listBooks = bookService.list();

		// send the list of books
		return ResponseEntity.ok().body(listBooks);

	}

	// Save the book
	@PostMapping("/api/book")
	public ResponseEntity<?> save(@RequestBody Book book) {

		// Save the book
		long id = bookService.save(book);

		return ResponseEntity.ok().body("Book created with the id: " + id);
	}

	// Get specific book record
	@GetMapping("/api/book/{bookId}")
	public ResponseEntity<?> getBook(@PathVariable("bookId") long bookId) {

		// Save the book
		Book myBook = bookService.get(bookId);

		return ResponseEntity.ok().body(myBook);
	}

	// Save the book record
	@PutMapping("/api/book/{bookId}")
	public ResponseEntity<?> updateBook(@PathVariable("bookId") long bookId, @RequestBody Book book) {

		// Update the book data
		bookService.update(bookId, book);

		return ResponseEntity.ok().body("Book has been updated!");
	}

	// Delete the book record
	@DeleteMapping("/api/book/{bookId}")
	public ResponseEntity<?> deleteBook(@PathVariable("bookId") long bookId) {

		// Update the book data
		bookService.delete(bookId);

		return ResponseEntity.ok().body("The Book with ID: " + bookId + " has been Deleted!");
	}

}
