package pt.jorge;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
public class SpringAngularAppApplication extends SpringBootServletInitializer {

	/*
	 * // used to run as a normal class public class SpringAngularAppApplication{
	 * public static void main(String[] args) {
	 * SpringApplication.run(SpringAngularAppApplication.class, args); }
	 * 
	 */
}
