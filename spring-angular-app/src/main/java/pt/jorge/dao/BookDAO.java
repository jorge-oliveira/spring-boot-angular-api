package pt.jorge.dao;

import java.util.List;

import pt.jorge.model.Book;

public interface BookDAO {

	// Save the record
	long save(Book book);
	
	// Get a single record
	Book get(long id);
	
	// Get all the records
	List<Book> list();
	
	// Update the record
	void update(long id, Book book);
	
	// Delete the record
	void delete(long id);
}
