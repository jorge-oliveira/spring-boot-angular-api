package pt.jorge.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import pt.jorge.model.Book;
import pt.jorge.repository.BookRepository;

@Repository
public class BookDAOImpl implements BookDAO {

	@Autowired
	private BookRepository bookRepository;

	@Override
	public long save(Book book) {
		bookRepository.save(book);
		return book.getId();
	}

	@Override
	public Book get(long id) {

		Optional<Book> theBook = bookRepository.findById(id);
		return theBook.get();
	}

	@Override
	public List<Book> list() {
		return this.bookRepository.findAll();
	}

	@Override
	public void update(long id, Book book) {

		Optional<Book> bookDb = this.bookRepository.findById(id);

		Book bookUpdate = bookDb.get();
		bookUpdate.setId(id);
		bookUpdate.setTitle(book.getTitle());
		bookUpdate.setAuthor(book.getAuthor());

		bookRepository.saveAndFlush(bookUpdate);

	}

	@Override
	public void delete(long id) {

		Optional<Book> bookDb = this.bookRepository.findById(id);

		bookRepository.delete(bookDb.get());
	}

}
