import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Book } from './book';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';

@Injectable()
export class BookService {

    private bookUrl: string = 'http://localhost:8080/spring-bookapi/api/book/';  // URL to web api

    constructor(private _httpService: HttpClient) { }

    // Get all books
    getAllBooks(): Observable<Book[]> {
        return this._httpService.get<Book[]>(this.bookUrl);
    }

    private handleError(error: Response) {
        return Observable.throw(error);
    }

    // add a new book passing a book object
    addBook(book: Book) {

        // convert book body into json format
        let body = JSON.stringify(book);

        // Adding custom HTTP Headers to requests
        let headers = new HttpHeaders({ 'Content-Type': 'application/json' });

        // define the http options
        let options = { headers, responseType: 'text' as 'text' };


        if (book.id) {
            // update the record
            return this._httpService.put(this.bookUrl + book.id, body, options);
        } else {

            // Make a post request
            return this._httpService.post(this.bookUrl, body, options);
        }

    }

    deleteBook(bookId: string) {
        // define the http options for the response
        let options = { responseType: 'text' as 'text' };

        return this._httpService.delete(this.bookUrl + bookId, options);
    }

    getBookById(bookId: string): Observable<Book> {
        return this._httpService.get<Book>(this.bookUrl + bookId);
    }
}