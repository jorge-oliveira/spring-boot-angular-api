import {Component, OnInit } from '@angular/core';
import {Book} from './book';
import { BookService } from './book.service';

@Component({
	selector : 'app-book',
	templateUrl: './book.component.html',
	styleUrls: ['./book.component.css']
})

export class BookComponent implements OnInit{

	books: Book[];

	book = new Book();

	constructor(private _bookService: BookService){}

	// override
	ngOnInit():void{
		this.getBooks();
	}

	getBooks(): void{
		this._bookService.getAllBooks()
		.subscribe((data) => {
			this.books = data, console.log(data)
		}, (error) => console.log(error) );
	}

	addBook():void{
		this._bookService.addBook(this.book)
		.subscribe(
			(response) => {	
				console.info("im here");
				console.log(response);
				// clean the form
				this.reset(); 
				// reload the page
				this.getBooks();
			},
			(error)  => { 
				console.error("I'm a error");
				console.log(error); 
				console.log(error.status);
			}
			)

	}

	private reset(){
		this.book.id=null;
		this.book.title=null;
		this.book.author=null;
	}

	deleteBook(bookId: string){
		this._bookService.deleteBook(bookId)
		.subscribe(
			(response) => {	
				console.log(response);
				// reload the page
				this.getBooks();
			},
			(error)  => { 
				console.log(error); 
			}
			)
	}

	getBookById(bookId: string){
		this._bookService.getBookById(bookId)
		.subscribe((data) => {
			this.book = data;
			console.log(data);
			this.getBooks();
		}, (error) => console.log(error) );
	}
}